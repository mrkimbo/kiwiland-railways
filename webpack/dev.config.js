var path = require('path');
var webpack = require('webpack');
var OpenBrowserPlugin = require('open-browser-webpack-plugin');

module.exports = {
    entry: './app/index.js',
    devtool: 'eval',
    output: {
        path: path.resolve(__dirname, '../dist'),
        filename: 'api.min.js',
        publicPath: '/assets/'
    },
    plugins: [
        new OpenBrowserPlugin({ url: 'http://localhost:3000/dist/'})
    ],
    module: {
        loaders: [
            {
                test: /.jsx?$/,
                loader: 'babel-loader',
                exclude: /node_modules/,
                query: {
                    presets: ['es2015']
                }
            },
            {
                test: /.txt?$/,
                loader: 'raw-loader',
                exclude: /node_modules/
            }
        ]
    }
};
