var path = require('path');
var webpack = require('webpack');

module.exports = {
    entry: './app/index.js',
    devtool: 'eval',
    output: {
        path: path.resolve(__dirname, '../dist'),
        filename: 'assets/api.min.js',
        publicPath: '/assets/'
    },
    module: {
        loaders: [
            {
                test: /.jsx?$/,
                loader: 'babel-loader',
                exclude: /node_modules/,
                query: {
                    presets: ['es2015']
                }
            },
            {
                test: /.txt?$/,
                loader: 'raw-loader',
                exclude: /node_modules/
            }
        ]
    }
};
