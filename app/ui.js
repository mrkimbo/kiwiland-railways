'use strict';

var api = new KiwiAPI();

api.on(KiwiAPI.Event.READY, function () {
    console.log('API Ready - init UI');

    populateSelects();
    addUIListeners();
});
api.on(KiwiAPI.Event.ERROR, function () {
    console.error(api.error);
    find('#results').className = 'error';
    find('#results .output p').textContent = api.error;
});
api.init();

var _elCache = {};
function find(selector) {
    if(!_elCache[selector]) {
        _elCache[selector] = document.querySelector(selector);
    }
    return _elCache[selector];
}
function findAll(selector) {
    return Array.prototype.slice.apply(document.querySelectorAll(selector));
}

function populateSelects() {
    var towns = api.TownNames;

    findAll('.towns').forEach(function (select) {
        var opts = '';
        towns.forEach(function (town) {
            opts += '<option>' + town + '</option>';
        });
        select.innerHTML = opts;
    });
}

function addUIListeners() {
    find('#submit-distance').addEventListener('click', findDistance);
    find('#submit-shortest').addEventListener('click', findShortestDistance);
    find('#submit-max-stops').addEventListener('click', function (evt) {
        find('#max-stops-exact').checked ?
            findRoutesWithExactStops() :
            findRoutesWithMaxStops();
    });
    find('#submit-max-distance').addEventListener('click', findRoutesWithinDistance);
    find('#predefined').addEventListener('click', runTestPackage);
}

function findDistance() {
    handleResult(api.findDistance(find('#distance input').value));
}

function findShortestDistance() {
    handleResult(api.findShortestDistance(
        find('#shortest-from').value,
        find('#shortest-to').value
    ));
}

function findRoutesWithExactStops() {
    handleResult(
        api.findRoutesWithExactStops(
            find('#max-stops-from').value,
            find('#max-stops-to').value,
            find('#max-stops-count').value,
            find('#max-stops-exact').checked
        )
    );
}

function findRoutesWithMaxStops() {
    handleResult(
        api.findRoutesWithMaxStops(
            find('#max-stops-from').value,
            find('#max-stops-to').value,
            find('#max-stops-count').value,
            find('#max-stops-exact').checked
        )
    );
}

function findRoutesWithinDistance() {
    handleResult(api.findRoutesWithinDistance(
        find('#max-distance-from').value,
        find('#max-distance-to').value,
        find('#max-distance-count').value
    ));
}

function runTestPackage() {

    var results = [
        api.findDistance('ABC'),
        api.findDistance('AD'),
        api.findDistance('ADC'),
        api.findDistance('AEBCD'),
        api.findDistance('AED'),

        api.findRoutesWithMaxStops('C', 'C', 3, false),
        api.findRoutesWithMaxStops('A', 'C', 4, true),

        api.findShortestDistance('A', 'C'),
        api.findShortestDistance('B', 'B'),

        api.findRoutesWithinDistance('C', 'C', 30)
    ];

    handleResult(results.join('<br/>'));
}

function handleResult(result) {
    if (!result) return;

    find('#results').className = '';
    find('#results .output p').innerHTML = result;
}
