'use strict';

/**
 * Sanitise user input text
 * @param {string} inputStr
 */
export function sanitiseString(inputStr) {
    return inputStr.toUpperCase().replace(/\W-/g);
}

/**
 * Sanitise user input number
 * @param {string|number} numStr
 * @returns {Number}
 */
export function sanitiseNumber(numStr) {
    return parseInt(numStr);
}
