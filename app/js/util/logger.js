'use strict';

function _log(level, ...args) {
    // Don't log inside Node env (mainly for tests)
    if(this === global) return;
    console[level](...args);
}

export function log(...args) {
    _log('log', ...args);
}

export function warn(...args) {
    _log('warn', ...args);
}

export function error(...args) {
    _log('error', ...args);
}
