'use strict';

export function handleError(err) {
    console.error(err);
}

export function handleWarning(message) {
    console.warn(message);
}
