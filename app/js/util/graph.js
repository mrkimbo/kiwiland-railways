'use strict';

import { Errors } from '../constants/errors';
import { Promise } from 'es6-promise';

/**
 * Sanitise input graph and return array of entries
 * @param {string} graph
 * @returns {Array}
 */
export function sanitiseGraph(graph) {
    let entries = graph.toUpperCase().replace(/[^a-z,0-9,]/gi,'');

    if(!entries.length) {
        throw new Error(Errors.GRAPH_PARSE);
    }
    return entries;
}

/**
 * Parse individual entries - check for malformed items
 * @param {string} entryStr
 * @returns {Array}
 */
export function parseGraphEntries(entryStr) {
    const entries = entryStr.split(',');
    if(entries.some((item) => !isValidEntry(item))) {
        throw new Error(Errors.GRAPH_MALFORMED_NODE);
    }

    return entries;
}

/**
 * Test if entry is valid:
 * - 2 UpperCase letters, followed by a non-zero digit, then optionally more digits
 * - First 2 letters cannot be the same.
 * @param {string} entry
 * @returns {boolean}
 */
export function isValidEntry(entry) {
    return (/^[A-Z]{2}[1-9](\d?)*$/i).test(entry) &&
        entry.charAt(0) !== entry.charAt(1);
}

/**
 * Test for duplicate journey
 * @param {Object} journeys
 * @param {string} journeyName
 * @returns {boolean}
 */
export function journeyExists(journeys, journeyName) {
    return !!journeys[journeyName];
}

