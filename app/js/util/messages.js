'use strict';

// ToDo: Write tests

import { CommandTypes } from '../commands/index';
import { Warnings } from '../constants/errors';

const templates = {
    DISTANCE: 'The distance of journey {journey} is: {result}',
    QUICKEST: 'The shortest journey from {from} to {to} is: {result}',
    ROUTE_COUNT_EXACT: 'The number of journeys from {from} to {to} with exactly {stops} stops is: {result}',
    ROUTE_COUNT_MAX: 'The number of journeys from {from} to {to} with {stops} or less stops is: {result}',
    ROUTE_COUNT_DISTANCE: 'The number of journeys from {from} to {to} whose distance is less than {distance} is: {result}',
    UNKNOWN: 'Sorry, I did not understand the question....'
};

function supplant(str, o) {
    return str.replace(/{([^{}]*)}/g,
        function (a, b) {
            var r = o[b];
            return typeof r === 'string' || typeof r === 'number' ? r : a;
        }
    );
}

/**
 * Format and return a message
 * @param {string} type
 * @param {Object} values
 * @returns {string}
 */
export default function message(type, values) {

    // Message already set
    if(values.result === Warnings.JOURNEY_NOT_FOUND || values.result === undefined) {
        return values.result;
    }

    switch(type) {
        case CommandTypes.DISTANCE:
            return supplant(templates.DISTANCE, values);

        case CommandTypes.QUICKEST:
            return supplant(templates.QUICKEST, values);

        case CommandTypes.ROUTE_COUNT_STOPS:
            const template = values.exact ?
                templates.ROUTE_COUNT_EXACT : templates.ROUTE_COUNT_MAX;
            return supplant(template, values);

        case CommandTypes.ROUTE_COUNT_DISTANCE:
            return supplant(templates.ROUTE_COUNT_DISTANCE, values);

        default:
            return templates.UNKNOWN;
    }
}
