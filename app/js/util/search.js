'use strict';

/**
 * Split a given journey into routes
 * @param {string} journey
 */
export function getJourneyRoutes(journey) {

    return journey.split('')
        .map((item, idx, arr) => {
            if(idx < arr.length-1) {
                return item + arr[idx+1]
            }
        }).slice(0, -1);
}

/**
 * Helper method to check for repeated connections
 * @param {Array} legs
 * @param {string} route
 * @returns {number}
 */
export function getRepeatCount(legs, route) {
    return legs.reduce((count, item, idx, arr) => {
        if(idx<arr.length-1 && item + arr[idx+1] === route) count++;
        return count;
    }, 0);
}

/**
 * Helper method to return connecting routes
 * @param {Array} routes
 * @param {string} town
 * @returns {Array}
 */
export function getConnections(routes, town) {
    return routes.filter(item => {
        return item.charAt(0) === town.toUpperCase();
    }).slice();
}
