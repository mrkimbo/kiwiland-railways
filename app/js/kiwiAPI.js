'use strict';

import EventEmitter from 'events';
import App from './app';
import { CommandTypes } from './commands/index';
import Message from './util/messages';
import * as Logger from './util/logger';
import { sanitiseNumber, sanitiseString } from './util/input'
import Events from './constants/events';
import States from './constants/states';

let _app = undefined;

export default class KiwiAPI extends EventEmitter {

    constructor() {
        super();
    }

    init() {
        _app = new App();

        // proxy app events:
        _app.on(Events.APP_STATE_CHANGE, () => {
            switch (_app.state) {
                case States.READY:
                    return this.emit(Events.READY);

                case States.ERROR:
                    return this.emit(Events.ERROR, _app.error);
            }
        });

        _app.fetchData();
    }

    findDistance(journey) {
        const result = _app.query(CommandTypes.DISTANCE, sanitiseString(journey));
        return Message(CommandTypes.DISTANCE, {
            journey, result
        });
    }

    findShortestDistance(from, to) {
        const result = _app.query(CommandTypes.QUICKEST,
            sanitiseString(from), sanitiseString(to)
        );
        return Message(CommandTypes.QUICKEST, {
            from, to, result
        });
    }

    findRoutesWithMaxStops(from, to, stops) {
        const exact = false;
        const result = _app.query(CommandTypes.ROUTE_COUNT_STOPS,
            sanitiseString(from), sanitiseString(to), sanitiseNumber(stops), exact
        );
        return Message(CommandTypes.ROUTE_COUNT_STOPS, {
            from, to, stops, exact, result
        });
    }

    findRoutesWithExactStops(from, to, stops) {
        const exact = true;
        const result = _app.query(CommandTypes.ROUTE_COUNT_STOPS,
            sanitiseString(from), sanitiseString(to), sanitiseNumber(stops), exact
        );
        return Message(CommandTypes.ROUTE_COUNT_STOPS, {
            from, to, stops, exact, result
        });
    }

    findRoutesWithinDistance(from, to, distance) {
        const result = _app.query(CommandTypes.ROUTE_COUNT_DISTANCE,
            sanitiseString(from), sanitiseString(to), sanitiseNumber(distance)
        );
        return Message(CommandTypes.ROUTE_COUNT_DISTANCE, {
            from, to, distance, result
        });
    }

    get TownNames() {
        return _app.townNames;
    }

    get state() {
        return _app.state;
    }

    get error() {
        return 'KiwiAPI::Error - ' + _app.error.message;
    }

    // expose copy of internal states list
    static get State() {
        return JSON.parse(JSON.stringify(States));
    }

    // expose copy of internal events list
    static get Event() {
        return {
            READY: Events.READY,
            ERROR: Events.ERROR
        };
    }

    static get version() {
        return _app.version;
    }

    toString() {
        return 'Kiwiland Railways API ' + _app.version;
    }
}
