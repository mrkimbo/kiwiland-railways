'use strict';

import EventEmitter from 'events';
import DataService from './core/dataService';
import Model from './models/journeyModel';
import States from './constants/states';
import Events from './constants/events';
import { Errors } from './constants/errors';
import * as Commands from './commands/index';
import * as Logger from './util/logger';

const _version = '1.0.0';

function _clone(obj) {
    return JSON.parse(JSON.stringify(obj));
}

export default class App extends EventEmitter {

    constructor() {
        super();

        this._state = States.INIT;
        this._error = undefined;
    }

    fetchData() {
        DataService.fetch('./assets/journey-graph.txt')
            .then((data) => {
                this.init(data.text);
            }, (err) => {
                this.error = new Error(Errors.GRAPH_LOAD_FAILED);
            });
    }

    init(journeyGraph) {
        // ToDo: display init errors in output html
        try {
            Model.deserialize(journeyGraph);
            this.ready();
        } catch (err) {
            this.error = err;
        }
    }

    ready() {
        this._state = States.READY;
        this.emit(Events.APP_STATE_CHANGE);
    }

    /**
     * Execute a command (synchronously)
     * @param {string} command
     * @param {...rest} args
     * @returns {*}
     */
    query(command, ...args) {
        let result;

        const Cmd = Commands[command];
        try {
            //Logger.log(this + '::query()', ...args);
            result = new Cmd(...args).execute();
        } catch (err) {
            this.error = err;
        }

        return result;
    }

    get townNames() {
        let a, b;
        return Model.getRouteKeys().reduce((store, item) => {
            [a, b] = item.split('');
            if(!store.includes(a)) store[store.length] = a;
            if(!store.includes(b)) store[store.length] = b;
            return store;
        }, []);
    }

    // State/Error accessors - (no setter for state)
    get error() {
        return this._error;
    }
    set error(err) {
        this._error = err;
        this._state = States.ERROR;
        this.emit(Events.APP_STATE_CHANGE);
    }
    get state() {
        return this._state;
    }

    // expose copy of internal states list
    get states() {
        return _clone(States);
    }

    // expose copy of internal events list
    get event() {
        return _clone(Events);
    }

    get version() {
        return _version;
    }

    toString() {
        return 'App';
    }
}
