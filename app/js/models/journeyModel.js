'use strict';

import * as GraphUtil from '../util/graph';
import { handleError, handleWarning } from '../util/errors'
import Logger from '../util/logger';
import { Errors, Warnings } from '../constants/errors';

let _routes, _routeKeys;

/**
 * Journey Data Store.
 * Expose route information, but try to keep as logic-free as possible.
 */
class JourneyModel {

    constructor() {
        _routes = {};
        _routeKeys = [];
    }

    /**
     * Sanitise and parse journey entries from supplied text file.
     * @param {string} graph
     */
    deserialize(graph) {
        let entries = undefined;

        entries = GraphUtil.parseGraphEntries(
            GraphUtil.sanitiseGraph(graph)
        );

        // add entries to collection
        entries.reduce((items, entry) => {
            const [ townA, townB ] = entry.split('');
            items[townA + townB] = parseInt(entry.substr(2));

            return items;
        }, _routes);

        _routeKeys = Object.keys(_routes);
    }

    /**
     * Public accessor for route list
     * @returns {Object}
     */
    getRoutes() {
        return _routes;
    }

	/**
     * Public accessor for route names
     * @returns {*}
     */
    getRouteKeys() {
        return _routeKeys;
    }

    toString() {
        return 'JourneyModel';
    }
}

// export singleton
export default new JourneyModel();
