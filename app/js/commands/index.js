'use strict';

import AbstractCommand from './abstractCommand';
import FindDistanceCommand from './findDistanceCommand';
import FindShortestDistanceCommand from './findShortestDistanceCommand';
import FindDistanceLimitedRouteCountCommand from './findDistanceLimitedRouteCountCommand';
import FindRouteCountCommand from './findRouteCountCommand';

const CommandTypes = {
    DISTANCE: 'FindDistanceCommand',
    QUICKEST: 'FindShortestDistanceCommand',
    ROUTE_COUNT_STOPS: 'FindRouteCountCommand',
    ROUTE_COUNT_DISTANCE: 'FindDistanceLimitedRouteCountCommand'
};

export {
    AbstractCommand,
    FindDistanceCommand,
    FindShortestDistanceCommand,
    FindDistanceLimitedRouteCountCommand,
    FindRouteCountCommand,
    CommandTypes,
};
