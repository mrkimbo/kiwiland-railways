'use strict';

import AbstractCommand from './abstractCommand';
import Model from '../models/journeyModel';
import { Warnings, Errors } from '../constants/errors';
import Logger from '../util/logger';
import * as Search from '../util/search';

export default class FindDistanceCommand extends AbstractCommand {

    constructor(journey) {
        super();
        this._journey = journey;

        if (!journey) {
            throw new Error(Errors.EMPTY_OR_INVALID_INPUT);
        }
    }

    execute() {
        let routes, distances;

        routes = Search.getJourneyRoutes(this._journey);
        distances = routes.map(function (leg) {
            return Model.getRoutes()[leg] || Infinity;
        });

        // if any route routes not available, then return not-found
        if (distances.some(item => item === Infinity)) {
            return Warnings.JOURNEY_NOT_FOUND;
        }

        // calculate and return total distance
        return distances.reduce((dist, leg) => dist + leg);
    }

    toString() {
        return 'FindDistanceCommand';
    }

}
