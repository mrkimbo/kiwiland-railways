'use srict';

export default class AbstractCommand {

    execute() {
        throw new Error('Class not extended');
    }

    toString() {
        return 'AbstractCommand';
    }
}
