'use strict';

import AbstractCommand from './abstractCommand';
import Model from '../models/journeyModel';
import Logger from '../util/logger';
import * as Search from '../util/search';
import { Warnings, Errors } from '../constants/errors';

export default class FindDistanceLimitedRouteCountCommand extends AbstractCommand {

    constructor(from, to, maxDistance) {
        super();

        this._from = from;
        this._to = to;
        this._maxDistance = maxDistance;

        this._routes = {};

        this.mapRoutes = this.mapRoutes.bind(this);

        if (!from || !to || isNaN(maxDistance)) {
            throw new Error(Errors.EMPTY_OR_INVALID_INPUT);
        }
    }

    mapRoutes(legs, distance, routes) {
        let route, end, length;

        while (routes.length) {
            route = routes.shift();
            end = route.charAt(1);
            length = Model.getRoutes()[route] || Infinity;

            // check for distance threshold
            if (distance + length >= this._maxDistance) return;

            if (end === this._to) {
                this._routes[legs.concat(end).join('')] = distance + length;
            }

            // distance still under threshold, so continue search
            this.mapRoutes(
                legs.concat(end),
                distance + length,
                Search.getConnections(Model.getRouteKeys(), end));
        }
    }

    execute() {
        const startPoints = Search.getConnections(Model.getRouteKeys(), this._from);
        this.mapRoutes([this._from], 0, startPoints);

        return Object.keys(this._routes).length;
    }

    toString() {
        return 'FindDistanceLimitedRouteCountCommand';
    }
}
