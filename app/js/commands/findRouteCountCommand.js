'use strict';

import AbstractCommand from './abstractCommand';
import * as Search from '../util/search';
import Logger from '../util/logger';
import { Warnings, Errors } from '../constants/errors';
import Model from '../models/journeyModel';

export default class FindRouteCountCommand extends AbstractCommand {

    constructor(from, to, maxStops, exact = true) {
        super();

        this._from = from;
        this._to = to;
        this._maxStops = maxStops;
        this._exact = exact;
        this._routes = [];

        this.mapRoutes = this.mapRoutes.bind(this);

        if (!from || !to || isNaN(maxStops)) {
            throw new Error(Errors.EMPTY_OR_INVALID_INPUT);
        }
    }

    mapRoutes(legs, routes) {
        let route, end;

        while (routes.length) {
            route = routes.shift();
            end = route.charAt(1);

            if (end === this._to) {
                this._routes[this._routes.length] = legs.concat(end).join('');
            }
            if (legs.length < this._maxStops) {
                this.mapRoutes(
                    legs.concat(end),
                    Search.getConnections(Model.getRouteKeys(), end)
                );
            }
        }
    }

    execute() {
        const startPoints = Search.getConnections(Model.getRouteKeys(), this._from);
        this.mapRoutes([this._from], startPoints);

        if (this._exact) {
            this._routes = this._routes.filter(item => item.length === this._maxStops + 1);
        }

        return this._routes.length;
    }

    toString() {
        return 'FindRouteCountCommand';
    }
}
