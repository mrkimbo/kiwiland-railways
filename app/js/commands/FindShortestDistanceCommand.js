'use strict';

import AbstractCommand from './abstractCommand';
import * as Search from '../util/search';
import * as Logger from '../util/logger';
import Model from '../models/journeyModel';
import { Warnings, Errors } from '../constants/errors';

export default class FindShortestDistanceCommand extends AbstractCommand {

    constructor(from, to) {
        super();

        this._from = from;
        this._to = to;
        this._routeDistances = [];

        this.mapRoutes = this.mapRoutes.bind(this);

        if (!from || !to) {
            throw new Error(Errors.EMPTY_OR_INVALID_INPUT);
        }
    }

    mapRoutes(legs, distance, routes) {

        while (routes.length) {
            let route = routes.shift();
            let end = route.charAt(1);
            let length = Model.getRoutes()[route] || Infinity;

            if(length === Infinity) continue;

            // only allow same route twice in one journey
            if (Search.getRepeatCount(legs, route) > 2) {
                continue;
            }

            if (end === this._to) {
                this._routeDistances.push(distance + length);
            } else {
                this.mapRoutes(
                    legs.concat(end),
                    distance + length,
                    Search.getConnections(Model.getRouteKeys(), end)
                );
            }
        }
    }

    execute() {
        // Check for direct route:
        const min = Model.getRoutes()[this._from + this._to] || Infinity;;
        if (min !== Infinity) return min;

        // check route is achievable
        const startPoints = Search.getConnections(Model.getRouteKeys(), this._from);

        // create segments by inspecting possible routes.
        this.mapRoutes([this._from], 0, startPoints);

        // return lowest route distance
        const result = this._routeDistances.reduce((min, curr) => Math.min(min, curr), Infinity);

        return result === Infinity ?
            Warnings.JOURNEY_NOT_FOUND : result;
    }

    toString() {
        return 'FindShortestDistanceCommand';
    }
}
