'use strict';

export default {
    INIT: 'api:state:loading',
    READY: 'api:state:ready',
    ERROR: 'api:state:error'
};
