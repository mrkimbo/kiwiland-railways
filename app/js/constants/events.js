'use strict';

export default {

    APP_STATE_CHANGE: 'app:statechange',

    READY: 'app:event:ready',
    ERROR: 'app:event:error'

};
