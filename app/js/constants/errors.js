'use strict';

export const Errors = {

    GRAPH_LOAD_FAILED: 'Failed to load graph input',
    GRAPH_PARSE: 'Failed to parse input graph',
    GRAPH_MALFORMED_NODE: 'Found malformed node in input graph',

    EMPTY_OR_INVALID_INPUT: 'Value provided was empty or invalid'

};

export const Warnings = {

    GRAPH_DUPLICATE_JOURNEY: 'Found duplicate journey in input graph',
    JOURNEY_NOT_FOUND: 'JOURNEY NOT FOUND'

};
