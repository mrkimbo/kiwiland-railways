'use strict';

import StaticData from '../../data/journey-graph.txt';
import request from 'superagent';
import { Promise } from 'es6-promise';

class DataService {

    /**
     * Load data asynchronously from endpoint
     * @returns {Promise}
     */
    fetch(endPoint) {
        return new Promise((resolve, reject) => {
            request(endPoint)
                .end(function(err, response){
                    if(err) {
                        reject(err);
                    } else {
                        resolve(response);
                    }
                });
        });
    }

    toString() {
        return 'DataService';
    }
}

// Export singleton instance
export default new DataService();
