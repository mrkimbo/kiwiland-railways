'use strict';

import { polyfill } from 'es6-promise';

import KiwiAPI from './js/kiwiAPI';
import DataService from './js/core/dataService';


// apply required environment polyfills
function applyPolyFills() {
    polyfill();
}

applyPolyFills();
window.KiwiAPI = KiwiAPI;
