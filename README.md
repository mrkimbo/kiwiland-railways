# Kiwiland

## - A ThoughtWorks Technical Challenge.
This program aims to provide customers with varied information about the train line's available routes and journeys.

## Brief
The local commuter railroad services a number of towns in Kiwiland.  Because of monetary concerns, all of the tracks are 'one-way.'  That is, a route from Kaitaia to Invercargill does not imply the existence of a route from Invercargill to Kaitaia.  In fact, even if both of these routes do happen to exist, they are distinct and are not necessarily the same distance!
 
The purpose of this problem is to help the railroad provide its customers with information about the routes.  In particular, you will compute the distance along a certain route, the number of different routes between two towns, and the shortest route between two towns.

***
### Recommended System Requirements
Project was developed and tested in:

- Node ^5.1.0
- npm ^3.3.12
- latest Mac Chrome, Safari or Firefox

***
### Installation
##### Unzip source or clone repo and install npm dependencies.:
```
$ cd {projectFolder} 
$ npm install
```

### Running the project
The project distribution folder is '/dist'. A pre-built version is included in the repo. 
This command creates a simple server at http://localhost:3000 in order to view the files

```
$ npm run serve
```


### Running the Unit Tests (single run)
```
$ npm test
```

### ReBuilding the project

```
$ npm run deploy
```
If you get permission errors when running this command, use:
```
$ chmod +x build.sh
```

***
## Design

  This solution is written in two parts; an app with an API wrapper and an implementation program.
  
  The API acts as a Facade and exposes only the required functionality from the app behind it. In addition, 
  it sanitises the input before the core algorithms are executed and manages the converting raw result data 
  into user-friendly responses.
  Finally, it dispatches runtime events to notify consuming applications of state and errors.
  
  The functionality inside the app is modularised using the Command pattern, allowing for not only 
  simple addition of future operations but also provides streamlined testing. It also implements a common 
  interface via the execute method (enforced by a superclass), so the app can remain safely unaware of exactly which 
  commands are being executed. 
  Common functionality, shared by commands is provided through the util modules, which expose bite-sized pieces of 
  functionality instead of more bulky classes.
  
  The graph data is stored in a hash map inside a segregated Model, which consists solely of a 
  deserialiser method and an accessor for the route data and its keys.
  The concept is to keep the model as logic-free as possible, keeping the data pure, whilst 
  letting the program do the heavy lifting.

  The graph data is served from a static file and bundled into the app, but the DataService 
  was written to allow this to be updated to a live, asynchronous endpoint with minimal changes.
 
 
  The implementation program is a simple, vanilla HTML/JS UI wiring that creates an instance of the API, 
  waits for it to dispatch a 'ready' event and then calls methods on it, handing back the responses to the 
  HTML.
 
 

##### Assumptions

 - Journey source graph should parse - failure will abort program with an error.
 - Graph nodes will consist of three portions: Start Town, End Town, Distance. 
   Invalid nodes are removed during runtime parsing.
 - Towns are expected to be named with a single letter. 
 - There is no limitations for distances other than they must be numeric.
 - Town names will be validated only as consisting of a single letter and whether they exist within the source graph. 
   There is no reason to constrain to range as this would prevent scalability.
 - Shortest route and stop counting inputs are assumed to be achievable.

  
##### Limitations / Improvements
 
 - Where no other limitations are enforced, the route parser will only collect up to two iterations 
   of the same route within a single journey. This is to prevent infinite recursion by following
   a route in a circular pattern.
 - Possibly move message implementation outside of the API in order to keep it cleaner.
 - Some optimisation needed around the error/warning messages.
 - At present several of the algorithms share similarities and it should be possible to refactor 
   them into a single process.
 - UI could do with a whole lotta love!
