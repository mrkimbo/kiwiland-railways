#!/usr/bin/env bash

APP="app"
DIST="dist"
ASSETS="$DIST/assets"

if [ ! -d "$DIST" ]; then
  mkdir "$DIST"
fi
if [ ! -d "$ASSETS" ]; then
  mkdir "$ASSETS"
fi

# copy runtime files
cp "$APP/index.html" "$DIST"
cp "$APP/data/journey-graph.txt" "$APP/ui.js" "$ASSETS"

# compile less
./node_modules/.bin/lessc "$APP/styles/styles.less" -x > "$ASSETS/styles.css"

# compile js
NODE_ENV=production ./node_modules/.bin/webpack -p --config webpack/prod.config.js
