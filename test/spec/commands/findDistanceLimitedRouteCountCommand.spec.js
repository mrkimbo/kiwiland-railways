'use strict';

import * as Commands from '../../../app/js/commands/index';
import StaticData from '../../../app/data/journey-graph.txt';
import Model from '../../../app/js/models/journeyModel';
import { Errors } from '../../../app/js/constants/errors';

describe('Given an instance of FindDistanceLimitedRouteCountCommand', () => {

    function runCommand(...args) {
        return new Commands.FindDistanceLimitedRouteCountCommand(...args).execute();
    }

    beforeAll(function () {
        Model.deserialize(StaticData);
    });

    it('should implement execute', () => {
        let instance = new Commands.FindDistanceLimitedRouteCountCommand('A', 'B', 4);

        expect(instance.execute).toBeDefined();
    });

    describe('and it is NOT given one or more valid arguments', function () {

        it('should throw an "Invalid Value" error', function () {
            expect(function () {
                new Commands.FindDistanceLimitedRouteCountCommand();
            }).toThrow(new Error(Errors.EMPTY_OR_INVALID_INPUT));
        });
    });

    describe('and it is given valid arguments', function () {

        it('should return the correct number of routes between two towns within a maximum distance', function () {

            expect(runCommand('C', 'C', 30)).toEqual(7);
        });
    });
});
