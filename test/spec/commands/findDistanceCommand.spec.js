'use strict';

import * as Commands from '../../../app/js/commands/index';
import { Warnings, Errors } from '../../../app/js/constants/errors';
import Model from '../../../app/js/models/journeyModel';
import StaticData from '../../../app/data/journey-graph.txt';

describe('Given an instance of FindDistanceCommmand', () => {

    function runCommand(...args) {
        return new Commands.FindDistanceCommand(...args).execute();
    }

    beforeAll(function() {
        Model.deserialize(StaticData);
    });

    it('should implement execute', () => {
        let instance = new Commands.FindDistanceCommand('TEST');
        expect(instance.execute).toBeDefined();
    });

    describe('and it is NOT given a valid journey input', function() {

        it('should throw an "invalid input" error', function() {
            expect(function() {
                new Commands.FindDistanceCommand();
            }).toThrow(new Error(Errors.EMPTY_OR_INVALID_INPUT));
        })
    });

    describe('and it is given a valid journey input', function() {

        it('should return the correct distance for a given route', function () {
            expect(runCommand('ABC')).toEqual(9);
            expect(runCommand('AD')).toEqual(5);
            expect(runCommand('ADC')).toEqual(13);
            expect(runCommand('AEBCD')).toEqual(22);
            expect(runCommand('AED')).toEqual(Warnings.JOURNEY_NOT_FOUND);
        });
    });
});
