'use strict';

import * as Commands from '../../../app/js/commands/index';
import StaticData from '../../../app/data/journey-graph.txt';
import Model from '../../../app/js/models/journeyModel';
import { Errors } from '../../../app/js/constants/errors';

describe('Given an instance of FindShortestDistanceCommmand', () => {
    let instance;

    function runCommand(...args) {
        return new Commands.FindShortestDistanceCommand(...args).execute();
    }

    beforeAll(function () {
        Model.deserialize(StaticData);
    });

    it('should implement execute', () => {
        instance = new Commands.FindShortestDistanceCommand('A', 'B');
        expect(instance.execute).toBeDefined();
    });

    describe('and it is NOT given one or more valid arguments', function () {

        it('should throw an "Invalid Value" error', function () {
            expect(function () {
                new Commands.FindShortestDistanceCommand();
            }).toThrow(new Error(Errors.EMPTY_OR_INVALID_INPUT));
        });
    });

    describe('and it is given valid arguments', function () {

        it('should return the shortest distance for a given route', function () {
            // A-B => 9
            expect(runCommand('A', 'C')).toEqual(9);

            // A-E => 9
            expect(runCommand('B', 'B')).toEqual(9);
        });
    });
});
