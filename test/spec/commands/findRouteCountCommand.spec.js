'use strict';

import * as Commands from '../../../app/js/commands/index';
import StaticData from '../../../app/data/journey-graph.txt';
import Model from '../../../app/js/models/journeyModel';
import { Warnings, Errors } from '../../../app/js/constants/errors';

describe('Given an instance of FindRouteCountCommmand', () => {

    function runCommand(...args) {
        return new Commands.FindRouteCountCommand(...args).execute();
    }

    beforeAll(function () {
        Model.deserialize(StaticData);
    });

    it('should implement execute', () => {
        let instance = new Commands.FindRouteCountCommand('A', 'B', 4);

        expect(instance.execute).toBeDefined();
    });

    describe('and it is NOT given one or more valid arguments', function () {

        it('should throw an "Invalid Value" error', function () {
            expect(function () {
                new Commands.FindRouteCountCommand();
            }).toThrow(new Error(Errors.EMPTY_OR_INVALID_INPUT));
        });
    });

    describe('and it is given an invalid journey', function () {

        it('should return 0', function () {

            expect(runCommand('A', 'Z', 4, true)).toEqual(0);
        });
    });

    describe('and it is given a EXACT stop-count of 4', function () {

        it('should return the correct number of routes between two towns with exactly 4 stops', function () {

            expect(runCommand('A', 'C', 4, true)).toEqual(3);
            expect(runCommand('A', 'C', 4, false)).toEqual(6);
        });
    });

    describe('and it is given a MAXIMUM stop-count of 3', function () {

        it('should return the correct number of routes between two towns with 3 OR LESS stops', function () {

            expect(runCommand('C', 'C', 3, false)).toEqual(2);
        });
    });
});
