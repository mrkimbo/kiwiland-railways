'use strict';

import Model from '../../app/js/models/journeyModel';

describe('Given an instance of JourneyModel', () => {
    let validGraph = 'AY1, AB3, XY20, YZ101';
    let instance = Model;

    beforeEach(function() {
        instance.constructor();
    });

    it('should have an deserialize method', () => {
        expect(instance.deserialize).toBeDefined();
    });

    describe('and it has been populated with route data', function() {

        beforeEach(function() {
            instance.deserialize(validGraph);
        });

        it('should contain a valid collection of routes', function() {
            expect(instance.getRouteKeys()).toEqual(
                ['AY', 'AB', 'XY', 'YZ']
            );
        });

        it('should expose the correct distance for a stored route', function() {
            expect(instance.getRoutes()['YZ']).toEqual(101);
        });

    });

});

