'use strict';

import * as Search from '../../../app/js/util/search';
import Model from '../../../app/js/models/journeyModel';

describe('Given the getJourneyRoutes method', function () {

    it('should split the journey into separate routes', function () {

        expect(Search.getJourneyRoutes('AB')).toEqual(['AB']);
        expect(Search.getJourneyRoutes('ABC')).toEqual(['AB', 'BC']);
        expect(Search.getJourneyRoutes('WXYZ')).toEqual(['WX', 'XY', 'YZ']);
    });
});

describe('Given the getRepeatCount method', function () {

    it('should correctly report the number of times a route appears within a journey', function () {
        const journey = 'ABCDBDBCDBCA'.split('');

        expect(Search.getRepeatCount(journey, 'AC')).toEqual(0);
        expect(Search.getRepeatCount(journey, 'AB')).toEqual(1);
        expect(Search.getRepeatCount(journey, 'CD')).toEqual(2);
        expect(Search.getRepeatCount(journey, 'BC')).toEqual(3);
    });
});

describe('Given the getConnections method', function () {

    beforeAll(function () {
        Model.getRouteKeys = sinon.stub(Model, 'getRouteKeys').returns(['AB', 'AC', 'AE', 'BA', 'BC']);
    });

    it('should return any available routes that leave from the given town', function () {
        const routes = Model.getRouteKeys();

        expect(Search.getConnections(routes, 'A')).toEqual(['AB', 'AC', 'AE']);
        expect(Search.getConnections(routes, 'B')).toEqual(['BA', 'BC']);
    });
});

describe('Given the getRouteLength method', function () {

    beforeAll(function () {
        Model.getRoutes = sinon.stub().returns({ 'AB': 10 });
    });

    describe('and it is given a valid route', function () {

        it('should return the correct distance for that route', function () {
            const result = Model.getRoutes()['AB'] || Infinity;

            expect(result).toEqual(10);
        });
    });

    describe('and it is NOT given an valid route', function () {

        it('should return Infinity', function () {
            const result = Model.getRoutes()['XXX'] || Infinity;

            expect(result).toEqual(Infinity);
        });
    });
});

