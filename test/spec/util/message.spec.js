'use strict';

import Message from '../../../app/js/util/messages';
import { CommandTypes } from '../../../app/js/commands/index';


describe('Given the message method', function () {

    describe('and it is NOT given a valid CommandType', function () {

        it('should return a friendly, generic error message', function () {

            expect(Message('TEST-COMMAND', {
                journey: 'TEST',
                result: 10
            })).toEqual('Sorry, I did not understand the question....');
        });
    });

    describe('and it is given a valid CommandType', function () {

        it('should return the correct message for that CommandType', function () {

            expect(Message(CommandTypes.DISTANCE, {
                journey: 'TEST',
                result: 10
            })).toEqual('The distance of journey TEST is: 10');
        });

        describe('and it is given a valid result', function () {

            it('should replace the appropriate tokens with the provided values', function () {
                expect(Message(CommandTypes.DISTANCE, {
                    journey: 'TEST',
                    result: 10
                })).toEqual('The distance of journey TEST is: 10');
            });
        });
    });

});
