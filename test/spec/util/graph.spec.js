'use strict';

import * as GraphUtils from '../../../app/js/util/graph';
import { Errors } from '../../../app/js/constants/errors';

let validGraph = ' Ab9,    bd11   ';
let invalidGraph = 'pp!!, #35-=, ++';

describe('Given the sanitiseGraph method', function () {

    describe('and it is passed an input graph', function () {

        it('should remove any invalid characters', function () {
            const output = GraphUtils.sanitiseGraph(invalidGraph);

            expect((/[^A-Z0-9,]/).test(output)).toEqual(false);
        });

        it('should force letters to uppercase', function () {
            const output = GraphUtils.sanitiseGraph(validGraph);

            expect(output).toEqual('AB9,BD11');
        });
    });

});

describe('Given the parseGraphEntries method', function () {

    describe('and it is passed a graph containing invalid entries', function () {

        it('should throw a "Malformed Node" error', function () {
            const entries = GraphUtils.sanitiseGraph(invalidGraph);

            expect(function () {
                GraphUtils.parseGraphEntries(entries)
            }).toThrow(new Error(Errors.GRAPH_MALFORMED_NODE));
        });
    });

    describe('and it is passed a graph containing valid entries', function () {

        it('should return an array of valid entries', function () {
            const entries = GraphUtils.parseGraphEntries(
                GraphUtils.sanitiseGraph(validGraph)
            );

            expect(entries).toEqual(['AB9', 'BD11']);
        });
    });

});

describe('Given the validEntry method', function () {

    it('should validate an entry correctly', function () {
        expect(GraphUtils.isValidEntry('')).toEqual(false);
        expect(GraphUtils.isValidEntry('B1')).toEqual(false);
        expect(GraphUtils.isValidEntry('BB0')).toEqual(false);
        expect(GraphUtils.isValidEntry('ABC')).toEqual(false);
        expect(GraphUtils.isValidEntry(' BC10')).toEqual(false);
        expect(GraphUtils.isValidEntry('BB12')).toEqual(false);
        expect(GraphUtils.isValidEntry('FA1000000')).toEqual(true);
        expect(GraphUtils.isValidEntry('AB2')).toEqual(true);
        expect(GraphUtils.isValidEntry('FA1000000')).toEqual(true);
    });

});
