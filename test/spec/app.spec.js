'use strict';

import App from '../../app/js/app';

describe('Given an instance of Api', () => {
    let instance = new App();

    it('should have an init method', () => {
        expect(instance.init).toBeDefined();
    });
});
