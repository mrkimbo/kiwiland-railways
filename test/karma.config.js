var webpack = require('karma-webpack');

module.exports = function (config) {
    config.set({
        frameworks: ['jasmine', 'sinon'],
        files: [
            '../node_modules/phantomjs-polyfill/bind-polyfill.js',
            './spec/**/*.spec.js'
        ],
        plugins: [
            webpack,
            'karma-sinon',
            'karma-jasmine',
            'karma-phantomjs-launcher',
            'karma-spec-reporter'
        ],
        browsers: ['PhantomJS'],
        preprocessors: {
            'spec/**/*.spec.js': ['webpack'],
            '../app/js/**/*.js': ['webpack']
        },
        reporters: ['spec'],
        webpack: {
            module: {
                loaders: [
                    {
                        test: /\.(js|jsx)$/,
                        exclude: /(bower_components|node_modules)/,
                        loader: 'babel-loader'
                    },
                    {
                        test: /\.txt$/,
                        exclude: /(bower_components|node_modules)/,
                        loader: 'raw-loader'
                    }
                ]
            }
        },
        webpackMiddleware: {noInfo: true}
    });
};
